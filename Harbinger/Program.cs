﻿using System;
using System.IO;
using System.Windows.Forms;

namespace Harbinger
{
    class Program
    {
        static void Main(string[] args)
        {
            if (args.Length == 1)
                if (args[0].ToLower() == "-version")
                {
                    Console.WriteLine(String.Format("{0}, written by {1}", Application.ProductName, Application.CompanyName));

                    Environment.Exit(0);
                }

            DirectoryInfo securityCheck = new DirectoryInfo(AppDomain.CurrentDomain.BaseDirectory);
            if (securityCheck.Name != "Harvest")
            {
                Console.WriteLine("Error. For security, the parent folder's name must be \"Harvest\".");
                Console.ReadKey();
                Environment.Exit(0);
            }

            foreach (string folder in Directory.GetDirectories(AppDomain.CurrentDomain.BaseDirectory))
            {
                string[] logFiles = Directory.GetFiles(folder, "*.log", SearchOption.TopDirectoryOnly);
                foreach (string logFile in logFiles)
                    File.Delete(logFile);

                foreach (string subfolder in Directory.GetDirectories(folder))
                    Misc_Utils.MoveDirectory(folder, AppDomain.CurrentDomain.BaseDirectory);
            }

            foreach (string pkgFile in Directory.GetFiles(AppDomain.CurrentDomain.BaseDirectory, "*.pkg", SearchOption.TopDirectoryOnly))
                File.Delete(pkgFile);
        }
    }
}
